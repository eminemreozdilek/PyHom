# HomPy

Last Release Date: 19.05.2023

## Contributing Authors

Emin Emre Ozdilek

Egecan Ozcakar

Nitel Muhtaroglu

Ugur Simsek

Orhan Gulcan

Gullu Kiziltas Sendur


## About Project

This project is a python code of 3D Homogenization. To increase the integration potential and adaptability of the homogenization approach to other computing packages and target adoption by a wider audience by leveraging the advantages of basing the solution on a free and open-source platform.

The Code also has functions to do Multi-Material and Thermal Homogenizations.

## Getting started

To Start Homogenization, parameters must be given which are "-s <size> -f <topology file> -e <elements per edge> -d <direct solution> -v <visual results>". Topology file can be can be editted as properties of struts, nodes and materials.

