import os

import numpy as np
import node
import material as m
import strut


def read_input_file(filename):
    input_file = open(filename, "r")
    lines = input_file.readlines()

    node_lookup_table = {}
    strut_lookup_table = {}
    material_lookup_table = {}
    data = [x.split() for x in lines]

    for line in data:
        if len(line) > 0:
            if (line[0]).lower() == "node":
                node_lookup_table[int(line[1])] = node.Node(int(line[1]), float(line[2]), float(line[3]),
                                                            float(line[4]))
            if (line[0]).lower() == "mat":
                new_material = m.Material(int(line[1]), float(line[2]), float(line[3]), float(line[4]))
                material_lookup_table[new_material.get_contains()] = new_material
    for line in data:
        if len(line) > 0:
            if (line[0]).lower() == "strut":
                strut_lookup_table[int(line[1])] = strut.Strut(int(line[1]), node_lookup_table[int(line[2])],
                                                               node_lookup_table[int(line[3])],
                                                               material_lookup_table[tuple([int(line[4])])],
                                                               float(line[5]))

    input_file.close()

    return node_lookup_table, strut_lookup_table, material_lookup_table


def generate_voxel(n, filename):
    size = 1 / n
    voxel = np.zeros((n, n, n))
    voxel_centers = np.zeros((n ** 3, 6))
    p = 0
    for i in range(n):
        for j in range(n):
            for k in range(n):
                voxel_centers[p][0] = k
                voxel_centers[p][1] = j
                voxel_centers[p][2] = i
                voxel_centers[p][3] = (k + 0.5) * size
                voxel_centers[p][4] = (j + 0.5) * size
                voxel_centers[p][5] = (i + 0.5) * size
                p = p + 1

    nodes, struts, materials = read_input_file(filename)
    np.seterr(invalid='ignore')
    for i in range(p):
        contained = []
        index1 = int(voxel_centers[i, 0])
        index2 = int(voxel_centers[i, 1])
        index3 = int(voxel_centers[i, 2])
        for key in struts:
            strut = struts[key]
            voxel_center = voxel_centers[i, 3:6]
            distance = calculate_distance(voxel_center, strut.get_start_node(), strut.get_end_node())
            radius = strut.get_radius()
            if (distance < radius) or np.isclose(distance, radius, 1.e-12, 1.e-12):
                contained.append(strut.get_material().get_name())
        contained_materials = tuple([*set(contained)])

        if len(contained_materials) == 0:
            pass
        elif contained_materials in materials:
            voxel[index1, index2, index3] = materials[contained_materials].get_name()
        else:
            materials = create_a_new_material(materials, contained_materials)
            voxel[index1, index2, index3] = materials[contained_materials].get_name()

    density = calculate_density(voxel, n)
    lambdas, mus, thermals = characterize(materials)
    return voxel, density, lambdas, mus, thermals


def calculate_distance(voxel_center, start_node, end_node):
    start_node = start_node.get_coordinate_array()
    end_node = end_node.get_coordinate_array()
    alpha = np.rad2deg(
        np.arccos(np.clip((np.dot((voxel_center - start_node) / np.linalg.norm(voxel_center - start_node),
                                  (end_node - start_node).T / np.linalg.norm(end_node - start_node))), -1., 1.)))
    beta = np.rad2deg(np.arccos(np.clip((np.dot((voxel_center - end_node) / np.linalg.norm(voxel_center - end_node),
                                                (start_node - end_node).T) / np.linalg.norm(start_node - end_node)),
                                        -1., 1.)))

    if alpha < 90 and beta < 90:
        return np.linalg.norm(np.cross(end_node - start_node, voxel_center - start_node)) / np.linalg.norm(
            end_node - start_node)
    elif alpha >= 90:
        return np.linalg.norm(voxel_center - start_node)
    else:
        return np.linalg.norm(voxel_center - end_node)


def create_a_new_material(materials, contained_materials):
    modulus_of_elasticity = 0
    poissons_ratio = 0
    thermal_conductivity = 0

    for index in contained_materials:
        material = tuple([index])
        modulus_of_elasticity += materials[material].get_modulus_of_elasticity()
        poissons_ratio += materials[material].get_poissons_ratio()
        thermal_conductivity += materials[material].get_thermal_conductivity()
        modulus_of_elasticity = modulus_of_elasticity / len(contained_materials)
        poissons_ratio = poissons_ratio / len(contained_materials)
        thermal_conductivity = thermal_conductivity / len(contained_materials)

    materials[contained_materials] = m.Material(len(materials) + 1, modulus_of_elasticity, poissons_ratio,
                                                thermal_conductivity)

    return materials


def characterize(material_list):
    lambdas = []
    mus = []
    thermals = []
    for index in material_list:
        material = material_list[index]
        lambdas.append(get_lames_first_parameter(material.get_modulus_of_elasticity(), material.get_poissons_ratio()))
        mus.append(get_lames_second_parameter(material.get_modulus_of_elasticity(), material.get_poissons_ratio()))
        thermals.append(material.get_thermal_conductivity())
    return lambdas, mus, thermals


def calculate_density(voxel, n):
    return np.count_nonzero(voxel) * n ** (-3)


def get_lames_first_parameter(youngs_modulus, poissons_ratio):
    return youngs_modulus * poissons_ratio / ((1 + poissons_ratio) * (1 - 2 * poissons_ratio))


def get_lames_second_parameter(youngs_modulus, poissons_ratio):
    return youngs_modulus / (2 * (1 + poissons_ratio))
