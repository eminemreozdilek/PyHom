import argparse as arg
import time
import preprocessor as pre
import numerical_homogenization as nh
import postprocessor as post

if __name__ == "__main__":
    parser = arg.ArgumentParser()
    parser.add_argument("-s", "--size", type=float, required=True, help="Unit cell size")
    parser.add_argument("-f", "--filename", type=str, required=True, help="Geometry definition file")
    parser.add_argument("-e", "--elements", type=int, required=True, help="Elements per edge")
    parser.add_argument("-d", "--direct_solver", type=int, required=True, help="Use Direct Solver")
    parser.add_argument("-v", "--visual_output", type=int, required=True, help="Visualize")

    # define input parameters
    args = parser.parse_args()
    unit_cell_length_in_x_direction = args.size
    unit_cell_length_in_y_direction = args.size
    unit_cell_length_in_z_direction = args.size
    input_file = args.filename
    resolution = args.elements
    direct_solver = bool(args.direct_solver)
    visualize = bool(args.visual_output)

    # preprocess
    voxels, volume_fraction, lambdas, mus, thermal_conductivities = pre.generate_voxel(resolution, input_file)

    # homogenize
    homogenized_constitutive_matrix = nh.homogenize(unit_cell_length_in_x_direction,
                                                    unit_cell_length_in_y_direction,
                                                    unit_cell_length_in_z_direction, lambdas, mus, voxels,
                                                    direct_solution=direct_solver)
    data = (str(time.ctime()) + '\n\n' +
            'Size: ' + str(args.size) + '\n' +
            'Filename: ' + str(args.filename) + '\n' +
            'Resolution: ' + str(args.elements) + '\n' +
            'Density: ' + str(round(volume_fraction, 3)) + '\n\n' +
            'Homogenized Constitutive Matrix' + '\n\n' +
            str(homogenized_constitutive_matrix))
    f = open("homogenization_result.txt", "w")
    f.write(data)
    f.close()

    if visualize:
        post.get_visual_results(resolution, voxels, args.size, homogenized_constitutive_matrix, input_file)
