import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm


def transform(itr, tmx):
    ne = itr.size
    nd = itr.ndim

    if ne == 3:
        return

    otr = np.copy(itr)
    otr.fill(0.)
    otr = np.reshape(otr, ne)
    itr_tmp = np.copy(itr)
    itr_tmp = np.reshape(itr_tmp, ne)

    cne = np.cumprod(3 * np.ones(nd)) / 3

    for oe in range(ne):
        ioe = ((np.floor(oe / cne)) % 3).astype(int)
        for ie in range(ne):
            pmx = 1
            iie = ((np.floor(ie / cne)) % 3).astype(int)
            for id1 in range(nd):
                pmx = pmx * tmx[ioe[id1], iie[id1]]
            otr[oe] = otr[oe] + pmx * itr_tmp[ie]

    otr = np.reshape(otr, (3, 3, 3, 3))
    return otr


def change(w):
    """change the index 4 5 6 to 23 31 12"""
    if w < 3:
        a = w
        b = w
    elif w == 3:
        a = 1
        b = 2
    elif w == 4:
        a = 2
        b = 0
    elif w == 5:
        a = 0
        b = 1
    return a, b


def generate(homogenized_constitutive_matrix):
    C = np.full((3, 3, 3, 3), 0.)
    for i in range(6):
        for j in range(6):
            (a, b) = change(i)
            (c, d) = change(j)
            C[a, b, c, d] = homogenized_constitutive_matrix[i, j]
    for i in range(3):
        if i == 2:
            j = 0
        else:
            j = i + 1
        for m in range(3):
            if m == 2:
                n = 0
            else:
                n = m + 1
            C[j, i, n, m] = C[i, j, m, n]
            C[j, i, m, n] = C[i, j, m, n]
            C[i, j, n, m] = C[i, j, m, n]
            C[j, i, m, m] = C[i, j, m, m]
            C[m, m, j, i] = C[m, m, i, j]
    return C


def modulus(homogenized_constitutive_matrix):
    E = np.zeros((6, 1))
    try:
        S = np.linalg.inv(homogenized_constitutive_matrix)
    except np.linalg.LinAlgError:
        return E
    E[0] = 1 / S[0, 0]
    E[1] = 1 / S[1, 1]
    E[2] = 1 / S[2, 2]
    E[3] = 1 / S[3, 3]
    E[4] = 1 / S[4, 4]
    E[5] = 1 / S[5, 5]
    return E


def convert_to_matrix(C):
    CH = np.zeros((6, 6))
    for i in range(6):
        for j in range(6):
            a, b = change(i)
            c, d = change(j)
            CH[i, j] = C[a, b, c, d]
    return CH


def convert_spherical_coordinates_to_cartesian(azimuth, elevation, r):
    x = r * np.cos(elevation) * np.cos(azimuth)
    y = r * np.cos(elevation) * np.sin(azimuth)
    z = r * np.sin(elevation)
    return x, y, z


def write_effective_youngs_modulus_surface(homogenized_constitutive_matrix, resolution_for_visualization, filename):
    tensor = generate(homogenized_constitutive_matrix)
    """find the E1 in 360 degree x = 0:pi/180:2*pi;"""
    a = np.linspace(0, 2 * np.pi, num=resolution_for_visualization)
    e = np.linspace(-np.pi / 2, np.pi / 2, num=resolution_for_visualization)
    a, e = np.meshgrid(a, e)
    e1 = np.zeros(np.shape(a))
    for i in range(np.shape(a)[0]):
        for j in range(np.shape(a)[1]):
            """build transformation matrix"""
            trans_z = np.matrix(
                [[np.cos(a[i][j]), -np.sin(a[i][j]), 0.], [np.sin(a[i][j]), np.cos(a[i][j]), 0.], [0., 0., 1.]])
            trans_y = np.matrix(
                [[np.cos(e[i][j]), 0., np.sin(e[i][j])], [0., 1., 0.], [-np.sin(e[i][j]), 0., np.cos(e[i][j])]])
            """calculate the new tensor"""
            N_tensor = transform(tensor, trans_y * trans_z)
            """transform the tensor to 6*6"""
            N_CH = convert_to_matrix(N_tensor)
            """calculate the E1"""
            E = modulus(N_CH)
            e1[i, j] = E[0]
    x, y, z = convert_spherical_coordinates_to_cartesian(a, e, e1)
    x_max = np.max(x)
    y_max = np.max(y)
    z_max = np.max(z)
    c = np.sqrt(x ** 2 + y ** 2 + z ** 2)
    f = open(filename, "w")
    f.write("X,Y,Z,C\n")
    for i in range((np.shape(x)[0])):
        for j in range((np.shape(x)[1])):
            f.write(str(.5 * (x[i][j] / x_max + 1.)) + "," + str(.5 * (y[i][j] / y_max + 1.)) + "," + str(
                .5 * (z[i][j] / z_max + 1.)) + "," + str(c[i][j]) + "\n")
    f.close()


def write_mesh(number_of_nodes_along_each_axis, voxels, element_size, filename):
    file = open(filename, "w")
    file.write("<?xml version=\"1.0\"?>\n")
    file.write("<VTKFile type=\"StructuredGrid\" version=\"0.1\">\n")
    file.write("<StructuredGrid WholeExtent=\"0 " + str(number_of_nodes_along_each_axis - 1) + " 0 " + str(
        number_of_nodes_along_each_axis - 1) + " 0 " + str(
        number_of_nodes_along_each_axis - 1) + " 0 0\">\n")
    file.write("<Piece Extent=\"0 " + str(number_of_nodes_along_each_axis - 1) + " 0 " + str(
        number_of_nodes_along_each_axis - 1) + " 0 " + str(
        number_of_nodes_along_each_axis - 1) + " 0 0\">\n")

    # Write DataArray
    file.write("<CellData Scalars=\"Fill\">\n")
    file.write("<DataArray type=\"Int32\" Name=\"Fill\" format=\"ascii\" NumberOfComponents=\"1\">\n")
    for voxel in voxels:
        file.write(str(voxel) + "\t")
    file.write("\n")
    file.write("</DataArray>\n")

    # Close Data Part
    file.write("</CellData>\n")

    # Write Point Coords
    file.write("<Points>\n")
    file.write("<DataArray type=\"Float32\" NumberOfComponents=\"3\">\n")
    for i in range(0, number_of_nodes_along_each_axis):
        for j in range(0, number_of_nodes_along_each_axis):
            for k in range(0, number_of_nodes_along_each_axis):
                file.write(str(i * element_size) + "\t" + str(j * element_size) + "\t" + str(k * element_size) + "\t")

    file.write("\n")
    file.write("</DataArray>\n")

    # Close all tags
    file.write("</Points>\n")
    file.write("</Piece>\n")
    file.write("</StructuredGrid>\n")
    file.write("</VTKFile>\n")

    # Close File
    file.close()


def draw_graph(homogenized_constitutive_matrix, resolution_for_visualization):
    tensor = generate(homogenized_constitutive_matrix)
    """find the E1 in 360 degree x = 0:pi/180:2*pi;"""
    a = np.linspace(0, 2 * np.pi, num=resolution_for_visualization)
    e = np.linspace(-np.pi / 2, np.pi / 2, num=resolution_for_visualization)
    a, e = np.meshgrid(a, e)
    e1 = np.zeros(np.shape(a))
    for i in range(np.shape(a)[0]):
        for j in range(np.shape(a)[1]):
            """build transformation matrix"""
            trans_z = np.matrix(
                [[np.cos(a[i][j]), -np.sin(a[i][j]), 0.], [np.sin(a[i][j]), np.cos(a[i][j]), 0.], [0., 0., 1.]])
            trans_y = np.matrix(
                [[np.cos(e[i][j]), 0., np.sin(e[i][j])], [0., 1., 0.], [-np.sin(e[i][j]), 0., np.cos(e[i][j])]])
            """calculate the new tensor"""
            N_tensor = transform(tensor, trans_y * trans_z)
            """transform the tensor to 6*6"""
            N_CH = convert_to_matrix(N_tensor)
            """calculate the E1"""
            E = modulus(N_CH)
            e1[i, j] = E[0]
    x, y, z = convert_spherical_coordinates_to_cartesian(a, e, e1)

    V = np.sqrt(x ** 2 + y ** 2 + z ** 2)
    V_normalized = (V - np.min(V)) / (np.max(V) - np.min(V))

    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    surf = ax.plot_surface(x, y, z, facecolors=plt.cm.jet(V_normalized), linewidth=0, antialiased=False)
    ax.set_xlabel('x', fontsize=18)
    ax.set_ylabel('y', fontsize=18)
    ax.set_zlabel('z', fontsize=18)
    m = cm.ScalarMappable(cmap=cm.jet)
    m.set_array(V)
    ax.zaxis.set_major_formatter('{x:.02f}')
    fig.colorbar(m)
    return plt


def get_visual_results(resolution, voxels, size, homogenized_constitutive_matrix, input_file, use_matplotlib=False):
    number_of_nodes_along_each_axis = resolution + 1
    resolution_for_visualization = 300
    voxels = np.reshape(voxels, (resolution * resolution * resolution)).astype(int)
    element_size = size / resolution

    write_mesh(number_of_nodes_along_each_axis, voxels, element_size, os.path.splitext(input_file)[0] + ".vts")

    if use_matplotlib:
        draw_graph(homogenized_constitutive_matrix, resolution_for_visualization)
    else:
        write_effective_youngs_modulus_surface(homogenized_constitutive_matrix, resolution_for_visualization,
                                               os.path.splitext(input_file)[0] + ".csv")
