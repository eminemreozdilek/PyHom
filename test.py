import unittest
import numpy as np
import preprocessor as gv
import numerical_homogenization as nh


class MyTestCase(unittest.TestCase):

    def test_grid_value(self):
        unit_cell_length_in_x_direction = 1
        unit_cell_length_in_y_direction = 1
        unit_cell_length_in_z_direction = 1
        number_of_voxels_along_each_axis = 12
        voxels, density, lambdas, mus, thermals = gv.generate_voxel(number_of_voxels_along_each_axis,
                                                                    'topology/grid.txt')

        actual = nh.homogenize(unit_cell_length_in_x_direction, unit_cell_length_in_y_direction,
                               unit_cell_length_in_z_direction, lambdas, mus, voxels)
        expected = [
            [25.93502539506385, 2.902684598536871, 2.902684598536867, 3.28737669054571e-15, -9.120240912312283e-16,
             3.046716524901516e-15],
            [2.902684598536871, 25.93502539506385, 2.902684598536865, -2.783096154793455e-16, 3.303168455176964e-15,
             -3.207070026212122e-16],
            [2.902684598536867, 2.902684598536865, 25.93502539506384, 6.977247555758903e-16, -1.545129973810509e-16,
             8.153200337090993e-17],
            [2.877223114573562e-15, -1.083778656011877e-16, 7.947032517729297e-16, 2.207049100641859,
             -1.628471663073228e-16, 3.93545991557746e-15],
            [-6.479497114634386e-16, 2.806021101510892e-15, 5.923929263321475e-16, -4.617346002072642e-17,
             2.207049100641843, -6.416232223803962e-17],
            [3.257485427232698e-15, -4.87890977618477e-18, 4.692427002517263e-16, 3.973722087870831e-15,
             -5.687546719940033e-16, 2.207049100641844]]
        self.assertTrue(np.allclose(expected, actual))

    def test_face_center_value(self):
        unit_cell_length_in_x_direction = 1
        unit_cell_length_in_y_direction = 1
        unit_cell_length_in_z_direction = 1
        number_of_voxels_along_each_axis = 12
        voxels, density, lambdas, mus, thermals = gv.generate_voxel(number_of_voxels_along_each_axis,
                                                                    'topology/face_center.txt')

        actual = nh.homogenize(unit_cell_length_in_x_direction, unit_cell_length_in_y_direction,
                               unit_cell_length_in_z_direction, lambdas, mus, voxels)
        expected = [
            [13.115276503263457, 5.469603840802276, 5.469603840802278, 4.0497661647764804e-16, -4.727528047851481e-16,
             2.437280554516783e-15],
            [5.469603840802276, 13.115276503263452, 5.469603840802277, -1.5092094240998222e-16, -4.423900617578705e-16,
             1.1583344960292008e-16],
            [5.469603840802278, 5.469603840802277, 13.115276503263452, 8.521829075736065e-16, -1.0252949485313489e-15,
             9.697102113127984e-16],
            [2.512367484192035e-16, -5.553283527470754e-16, 6.873841773558098e-16, 4.875890681416116,
             -1.7816152199368052e-16, 1.894616191364107e-15],
            [-2.597206304189026e-16, -6.786944663499279e-16, -1.0455848816289968e-15, 5.4277871260055566e-18,
             4.875890681416112, -4.5102810375396984e-17],
            [2.3408280657799888e-15, 2.0504973587132103e-17, 8.216818864176896e-16, 2.017727348049836e-15,
             4.450649918052996e-17, 4.875890681416114]]
        self.assertTrue(np.allclose(expected, actual))

    def test_octet_value(self):
        unit_cell_length_in_x_direction = 1
        unit_cell_length_in_y_direction = 1
        unit_cell_length_in_z_direction = 1
        number_of_voxels_along_each_axis = 12
        voxels, density, lambdas, mus, thermals = gv.generate_voxel(number_of_voxels_along_each_axis,
                                                                    'topology/octet.txt')

        actual = nh.homogenize(unit_cell_length_in_x_direction, unit_cell_length_in_y_direction,
                               unit_cell_length_in_z_direction, lambdas, mus, voxels)
        expected = [
            [201.8292760740822, 81.5684683067935, 81.5684683067935, 1.766208707065786e-14, -8.141084377813468e-15,
             2.766536999487812e-14],
            [81.5684683067935, 201.8292760740823, 81.5684683067935, -2.394243657499739e-15, -1.110011605201522e-14,
             -5.179884299266746e-15],
            [81.5684683067935, 81.56846830679352, 201.8292760740823, 7.129821906481926e-15, -3.41499289784053e-15,
             -4.909267437014364e-16],
            [1.681865909562708e-14, -2.085788139427613e-15, 7.213657839469367e-15, 61.80637766126404,
             -4.753142324176451e-16, -1.90299707415742e-14],
            [-6.149486302120533e-15, -1.048808392564715e-14, -3.225826723796121e-15, 5.689893001203927e-16,
             61.80637766126404, 4.859339926971407e-15],
            [2.663841369709985e-14, -4.159866895392383e-15, 3.313321839115702e-16, -1.591245581480938e-14,
             4.620788343970283e-15, 61.80637766126402]]
        self.assertTrue(np.allclose(expected, actual))

    def test_cubic_center_value(self):
        unit_cell_length_in_x_direction = 1
        unit_cell_length_in_y_direction = 1
        unit_cell_length_in_z_direction = 1
        number_of_voxels_along_each_axis = 12
        voxels, density, lambdas, mus, thermals = gv.generate_voxel(number_of_voxels_along_each_axis,
                                                                    'topology/cubic_center.txt')

        actual = nh.homogenize(unit_cell_length_in_x_direction, unit_cell_length_in_y_direction,
                               unit_cell_length_in_z_direction, lambdas, mus, voxels)
        expected = [
            [109.2833800485177, 39.72095035220543, 39.72095035220544, 1.288330336510213e-14, -4.699799577290165e-15,
             1.906808044793706e-14],
            [39.72095035220543, 109.2833800485177, 39.72095035220543, -2.477510384346626e-15, -4.008458062004783e-15,
             -2.818925648462312e-15],
            [39.72095035220544, 39.72095035220543, 109.2833800485176, 3.130091671965651e-15, -2.311031140761477e-15,
             -4.163336342344337e-16],
            [1.105268220696898e-14, -2.472224898755759e-15, 3.506526666252618e-15, 36.88301849973173,
             -5.360295540768334e-16, -8.340225211844743e-15],
            [-3.450148153283372e-15, -3.927386844557179e-15, -2.011357660286484e-15, -3.517151847542976e-16,
             36.88301849973172, 2.133601455234224e-15],
            [1.834296603497876e-14, -2.01574867908505e-15, 1.318389841742373e-16, -6.655375035802269e-15,
             1.923428864097909e-15, 36.88301849973171]]
        self.assertTrue(np.allclose(expected, actual))

    def test_tesseract_value(self):
        unit_cell_length_in_x_direction = 1
        unit_cell_length_in_y_direction = 1
        unit_cell_length_in_z_direction = 1
        number_of_voxels_along_each_axis = 12
        voxels, density, lambdas, mus, thermals = gv.generate_voxel(number_of_voxels_along_each_axis,
                                                                    'topology/tesseract.txt')

        actual = nh.homogenize(unit_cell_length_in_x_direction, unit_cell_length_in_y_direction,
                               unit_cell_length_in_z_direction, lambdas, mus, voxels)
        expected = [
            [141.0184252837208, 48.68002506523383, 48.68002506523384, 1.385759454235191e-14, -6.424656813497354e-15,
             2.10322211036118e-14],
            [48.68002506523383, 141.0184252837208, 48.68002506523384, -2.399529143090606e-15, -6.573300931345116e-15,
             -3.550978955324524e-15],
            [48.68002506523384, 48.68002506523383, 141.0184252837208, 4.106740988940594e-15, -3.143373148578599e-15,
             -9.367506770274758e-16],
            [1.274688362674914e-14, -2.405112784278907e-15, 4.368901074247589e-15, 41.8559158964356,
             -1.726049858596923e-16, -9.70924729504219e-15],
            [-4.684512326658119e-15, -5.318879017779388e-15, -3.003836328979714e-15, 2.38524477946811e-16,
             41.85591589643559, 3.036416604262904e-15],
            [2.080453864738985e-14, -2.947295185684595e-15, 2.905661822261152e-16, -7.76603174129642e-15,
             2.65000694998907e-15, 41.85591589643558]]
        self.assertTrue(np.allclose(expected, actual))

    def test_compare_direct_vs_iterative_solving(self):
        unit_cell_length_in_x_direction = 1
        unit_cell_length_in_y_direction = 1
        unit_cell_length_in_z_direction = 1
        number_of_voxels_along_each_axis = 12
        voxels, density, lambdas, mus, thermals = gv.generate_voxel(number_of_voxels_along_each_axis,
                                                                    'topology/grid.txt')

        iterative_solvers_outcome = nh.homogenize(unit_cell_length_in_x_direction, unit_cell_length_in_y_direction,
                                                  unit_cell_length_in_z_direction, lambdas, mus, voxels,
                                                  direct_solution=False)
        direct_solvers_outcome = nh.homogenize(unit_cell_length_in_x_direction, unit_cell_length_in_y_direction,
                                               unit_cell_length_in_z_direction, lambdas, mus, voxels,
                                               direct_solution=True)
        self.assertTrue(np.allclose(iterative_solvers_outcome, direct_solvers_outcome))

    def test_thermal(self):
        unit_cell_length_in_x_direction = 1
        unit_cell_length_in_y_direction = 1
        unit_cell_length_in_z_direction = 1
        number_of_voxels_along_each_axis = 12
        voxels, density, lambdas, mus, thermal_conductivities = gv.generate_voxel(number_of_voxels_along_each_axis,
                                                                                  'topology/x.txt')

        actual = nh.apply_thermal_homogenization(unit_cell_length_in_x_direction, unit_cell_length_in_y_direction,
                                                 unit_cell_length_in_z_direction, thermal_conductivities, voxels,
                                                 thermal_conductivity_of_space=0.013)

        expected = [[90.00396, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 90.00396, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 90.00396, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]]

        self.assertTrue(np.allclose(expected, actual))


if __name__ == '__main__':
    unittest.main()
